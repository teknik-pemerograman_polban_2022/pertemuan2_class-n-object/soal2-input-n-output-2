import java.util.Scanner;

/**
 * Print Formatting
 * Each String is left-justified with trailing whitespace through the first 15
 * characters. The
 * leading digit of the integer is the 16th character, and each integer that was
 * less than 3 digits
 * now has leading zeroes.
 *
 * @author Aryo
 */
public class App {
    /**
     * Ini adalah modul utama yang akan mengeksekusi semua sintax
     * 
     * @param args unused
     * @throws Exception unused
     */
    public static void main(String[] args) throws Exception {

        Scanner kScanner = new Scanner(System.in);

        String[] StringInput = new String[3];
        int IntInput[] = new int[3];

        for (int i = 0; i < 3; i++) {
            StringInput[i] = kScanner.next();
            IntInput[i] = kScanner.nextInt();
        }
        kScanner.close();

        System.out.println("==========================");
        for (int i = 0; i < 3; i++) {
            System.out.printf("%-15s %03d\n", StringInput[i], IntInput[i]);
        }
        System.out.println("==========================");
    }
}
